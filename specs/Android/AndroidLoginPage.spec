Specification Heading
=====================
Created by sahabt on 20.08.2020

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.

THY PROJEM
--------------------
*"2" saniye bekle
*Konum istegi icin deny tikla
*"2" saniye bekle
*Cookie'leri kabul ete tikla
*"2" saniye bekle
*Hamburger Menude aramaya tikla
*Bilet Al tikla
*Tek Yon tikla
*Kalkis tikla
*Kalkis sec
*Varis tikla
*Varis sec
*Arama yap
*Ucus sec
*EcoFly sec
*Devam butonuna tikla
*Adini gir
*Soyadini gir
*Cinsiyet Sec
*Dogum Tarihini gir
*Email gir
*Vatandaslik Sec