package com.saha.THYProje;

import com.saha.VakifKatilim.selector.Selector;
import com.saha.VakifKatilim.selector.SelectorFactory;
import com.saha.VakifKatilim.selector.SelectorType;
import com.thoughtworks.gauge.AfterScenario;
import com.thoughtworks.gauge.BeforeScenario;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class HookImpl {
    private Logger logger = LoggerFactory.getLogger(getClass());
    protected static AppiumDriver<MobileElement> appiumDriver;
    protected static FluentWait<AppiumDriver<MobileElement>> appiumFluentWait;
    protected boolean localAndroid = true;
    protected static Selector selector;


    @BeforeScenario
    public void beforeScenario() throws MalformedURLException {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!Test!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        if (StringUtils.isEmpty(System.getenv("key"))) {
            if (localAndroid) {
                logger.info("Local Browser");
                DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
                desiredCapabilities
                        .setCapability("platformName", MobilePlatform.ANDROID);
                desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
                //desiredCapabilities.setCapability(MobileCapabilityType.UDID, "LGH870d82f54fb");

                desiredCapabilities
                        .setCapability(AndroidMobileCapabilityType.APP_PACKAGE,
                                "com.turkishairlines.mobile");
                desiredCapabilities
                        .setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,
                                "com.turkishairlines.mobile.ui.main.MainActivity");
                desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
             //   desiredCapabilities
              //          .setCapability(MobileCapabilityType.NO_RESET, true);
             //   desiredCapabilities
              //          .setCapability(MobileCapabilityType.FULL_RESET, false);
                desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 3000);
                // desiredCapabilities.setCapability("unicodeKeyboard", true);
                // desiredCapabilities.setCapability("resetKeyboard", false);
                URL url = new URL("http://127.0.0.1:4723/wd/hub");
                appiumDriver = new AndroidDriver(url, desiredCapabilities);
            } else {
                DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
                desiredCapabilities
                        .setCapability("platformName", MobilePlatform.IOS);
                desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
                desiredCapabilities
                        .setCapability(MobileCapabilityType.UDID, "80588adc1cc94b4757347e095d40299459fead50");
                desiredCapabilities
                        .setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.vakifkatilim.mobile");
                desiredCapabilities
                        .setCapability(MobileCapabilityType.DEVICE_NAME, "iPhonebatu");

                desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "13.6");
                //desiredCapabilities.setCapability(MobileCapabilityType.NO_RESET, true);
                //desiredCapabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
                desiredCapabilities.setCapability("connectHardwareKeyboard", false);

                desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 300);

                desiredCapabilities.setCapability("sendKeyStrategy", "setValue");

                URL url = new URL("http://127.0.0.1:4723/wd/hub");
                appiumDriver = new IOSDriver(url, desiredCapabilities);


            }
        } else {
            String hubURL = "http://hub.testinium.io/wd/hub";
            DesiredCapabilities capabilities = new DesiredCapabilities();
            System.out.println("key:" + System.getenv("key"));
            System.out.println("platform" + System.getenv("platform"));
            System.out.println("version" + System.getenv("version"));

            if (StringUtils.isEmpty(System.getenv("testID"))) {
                if (System.getenv("platform").equals("ANDROID")) {
                    capabilities.setCapability("key", System.getenv("key"));
                    capabilities
                            .setCapability(AndroidMobileCapabilityType.APP_PACKAGE,
                                    "com.vakifkatilim.mobil");
                    capabilities
                            .setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,
                                    "boa.android.mobilebranch.v2.ui.splash.ACSplash");
                    capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
                    capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
                    capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
                    // capabilities.setCapability("unicodeKeyboard", false);
                    //capabilities.setCapability("resetKeyboard", false);
                    logger.info("denemexxxx");
                    appiumDriver = new AndroidDriver(new URL(hubURL), capabilities);
                    localAndroid = true;
                } else {
                    // capabilities.setCapability(CapabilityType.PLATFORM, Platform.MAC);
                    capabilities.setCapability("usePrebuiltWDA", true);
                    //capabilities.setCapability("appium:maxTypeFrequency", 5);
                    capabilities.setCapability("key", System.getenv("key"));
                    capabilities.setCapability("waitForAppScript", "$.delay(1000);");
                    capabilities.setCapability("bundleId", "com.vakifkatilim.mobile");
                    capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60000);

                    capabilities.setCapability("usePrebuiltWDA", true); //TimeOut
                    appiumDriver = new IOSDriver(new URL(hubURL), capabilities);
                    localAndroid = false;
                }
            } else {

                capabilities.setCapability("testinium:key", "vakifkatilim:6564c72ab95e53eed6b6cdb52733e618");
                capabilities.setCapability("testinium:testID", System.getenv("testID"));
                capabilities.setCapability("testinium:takesScreenshot", "true"); // "false", "only_failure", "true"
                capabilities.setCapability("testinium:recordsVideo", true);
                System.out.println("testID:" + System.getenv("testID"));

                if ("ANDROID".equals(System.getenv("platform"))) {

                    capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.vakifkatilim.mobil");
                    capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "boa.android.mobilebranch.v2.ui.splash.ACSplash");
                    capabilities.setCapability("platformName", Platform.ANDROID);
                    capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
                    capabilities.setCapability(MobileCapabilityType.NO_RESET, true);
                    capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
                    appiumDriver = new AndroidDriver(new URL(hubURL), capabilities);
                    localAndroid = true;

                } else {

                    capabilities.setCapability("bundleId", "com.vakifkatilim.mobile");
                    capabilities.setCapability("platformName", Platform.IOS);
                    appiumDriver = new IOSDriver(new URL(hubURL), capabilities);
                    localAndroid = false;
                }


            }


        }
        selector = SelectorFactory
                .createElementHelper(localAndroid ? SelectorType.ANDROID : SelectorType.IOS);
        //appiumDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        appiumFluentWait = new FluentWait<AppiumDriver<MobileElement>>(appiumDriver);
        appiumFluentWait.withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofMillis(450))
                .ignoring(NoSuchElementException.class);
    }

    @AfterScenario
    public void afterScenario() {
        if (appiumDriver != null)
            appiumDriver.quit();
    }

}
